<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Produk;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        User::create([
            'name' => 'admin',
            'email' => 'admin@g',
            'password' => bcrypt('123'),
            'role' => 'admin',
        ]);

        User::create([
            'name' => 'admin2',
            'email' => 'admin2@g',
            'password' => bcrypt('123'),
            'role' => 'admin',
        ]);

        User::create([
            'name' => 'user',
            'email' => 'user@g',
            'password' => bcrypt('123'),
            'role' => 'user',
        ]);

        Produk::create([
            'nama' => 'spatttttu',
            'stok' => '12',
            'harga' => '300000',
            'deskripsi' => 'sepatu bekas konstanta',
            'foto' => 'sepatu.jpg',
        ]);

        Produk::create([
            'nama' => 'tas',
            'stok' => '12',
            'harga' => '200000',
            'deskripsi' => 'sepatu bekas konstanta',
            'foto' => 'tas.jpg',
        ]);

        Produk::create([
            'nama' => 'Laptop Asus',
            'stok' => '12',
            'harga' => '600000',
            'deskripsi' => 'sepatu bekas konstanta',
            'foto' => 'sekiro.jpg',
        ]);
    }
}
