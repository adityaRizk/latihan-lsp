<?php

namespace App\Http\Controllers\User;

use App\Models\User;
use App\Models\Produk;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    public function index(){
        $produk = Produk::all();

        $no = 1;
        $total = 0;
        return view('User/index', compact(['produk','no']));
    }

    public function show(string $id)
    {
        $produk = Produk::where('id',$id)->first();

        return view('User/produk/detail',compact('produk'));
    }
    public function masukKeranjang(string $id)
    {
        $user = Auth::user();
        $produk = Produk::where('id',$id)->first();

        $user->produk()->syncWithoutDetaching([
            $produk->id => ['qty' => 1]
        ]);

        return redirect('/user/keranjang');
    }

    public function deleteKeranjang(string $id){

        $user = Auth::user();
        $user->produk()->detach($id);
        return redirect('/user/keranjang');
    }

    public function keranjangMin(string $id){

        $user = Auth::user();

        $qty = $user->produk()->find($id)->pivot->qty - 1;
        $user->produk()->updateExistingPivot($id, ['qty' => 1]);
        return redirect('/user/keranjang');
    }

    public function keranjangPlus(string $id){

        $user = Auth::user();
        $qty = $user->produk()->find($id)->pivot->qty + 1;
        $user->produk()->updateExistingPivot($id, ['qty' => $qty]);
        return redirect('/user/keranjang');
    }

    public function keranjang()
    {
        // dd('asdasd');
        $user = User::with('produk')->where('id',Auth::id())->first();
        // $user->with('produk')->fisrt();
        // dd();
        $total = 0;
        foreach( $user->produk as $key => $value) {

            $total = $total + $value->pivot->qty * $value->harga;
        }


        // dd($total);
        return view('User.produk.keranjang', compact(['user','total']));

    }


}
