<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src="https://cdn.tailwindcss.com"></script>
</head>
<body>
    <h1 class="text-red-900 ">Admin page</h1>
    <h2>Selamat datang wahai, {{ auth()->user()->name }} </h2>
    <a href="/admin" class="text-blue-700">Halaman Admin</a>
    <a href="/admin/produk" class="text-blue-700">Halaman Produk</a><br>
    <a href="/logout" onclick="return confirm('yalin ingin keluarrr')">Logout</a>
</body>
</html>
