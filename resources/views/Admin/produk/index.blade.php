<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://cdn.tailwindcss.com"></script>
    <title>Document</title>
</head>
<body>
    <h1>Halaman Data Produk</h1>
    <a href="/admin/produk/create">Tambah Data</a>
    <table>
        <tr>
            <th>NO</th>
            <th>Name</th>
            <th>Stok</th>
            <th>Harga</th>
            <th >foto</th>
            <th width="200px" >Deskripsi</th>
            <th width="100px">Aksi</th>
        </tr>
        @forelse ($produk as $data)
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $data->nama }}</td>
                <td>{{ $data->stok }}</td>
                <td>{{ $data->harga }}</td>
                <td><img src="/storage/images/{{ $data->foto }}" width="40px" alt=""></td>
                <td>{{ $data->deskripsi }}</td>
                <td class="flex">
                    <a href="/admin/produk/edit/{{ $data->id }}">Edit</a>
                    <form action="/admin/produk/{{ $data->id }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" onclick="return confirm('yakin ingin menghapus produk')">Hapus</button>
                    </form>
                </td>
            </tr>
            @empty
                <h1>Data tidak ditemukan</h1>
            @endforelse
    </table>
    <a href="/admin/dashboard">Dashboard</a>
</body>
</html>
