<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://cdn.tailwindcss.com"></script>
    <title>Document</title>
</head>
<body>
    <h1>Edit produk</h1>
    <form action="/admin/produk/{{ $produk->id }}" method="POST">
        @method("PUT")
        @csrf
        <label for="nama">Nama</label>
        <input type="text" name="nama" id="nama" value="{{ $produk->nama }}"><br>
        @error('nama')
            <p>{{ $message }}</p>
        @enderror

        <label for="stok">Stok</label>
        <input type="text" name="stok" id="stok" value="{{ $produk->stok }}"><br>
        @error('stok')
            <p>{{ $message }}</p>
        @enderror

        <label for="harga">Harga</label>
        <input type="text" name="harga" id="harga" value="{{ $produk->harga }}"><br>
        @error('harga')
            <p>{{ $message }}</p><br>
        @enderror

        <label for="deskripsi">Deskripsi</label>
        <input type="text" name="deskripsi" id="deskripsi" value="{{ $produk->deskripsi }}"><br>
        @error('deskripsi')
            <p>{{ $message }}</p><br>
        @enderror

        <button type="submit">Edit</button>
    </form>
</body>
</html>
