<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://cdn.tailwindcss.com"></script>
    <title>Document</title>
</head>
<body>
    <h1>Halaman Data Admin</h1>
    <a href="/admin/create">Tambah Data</a>
    <table>
        <tr>
            <th>NO</th>
            <th>Name</th>
            <th>Email</th>
            <th>Aksi</th>
        </tr>
        @forelse ($admin  as $user)
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td class="flex">
                    <a href="/admin/edit/{{ $user->id }}">Edit</a>
                    <form action="/admin/{{ $user->id }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" onclick="return confirm('yakin ingin menghapus produk')">Hapus</button>
                    </form>
                </td>
            </tr>
            @empty
                <h1>Data tidak ditemukan</h1>
            @endforelse
    </table>
    <a href="/admin/dashboard">Dashboard</a>

</body>
</html>
