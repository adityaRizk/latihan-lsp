<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://cdn.tailwindcss.com"></script>
    <title>Document</title>
</head>
<body>
    <form action="" method="POST">
        @csrf
        <label for="name">Name</label>
        <input type="text" name="name" id="name" value="{{ old('name') }}"><br>
        @error('name')
            <p>{{ $message }}</p>
        @enderror

        <label for="email">Email</label>
        <input type="text" name="email" id="email" value="{{ old('email') }}"><br>
        @error('email')
            <p>{{ $message }}</p>
        @enderror

        <label for="password">Password</label>
        <input type="text" name="password" id="password" value="{{ old('password') }}"><br>
        @error('password')
            <p>{{ $message }}</p><br>
        @enderror

        <button type="submit">Tambah</button>
    </form>
</body>
</html>
