<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://cdn.tailwindcss.com"></script>
    <title>Document</title>
</head>
<body>
    <h1>User page</h1>
    <h2>Selamat datang wahai, {{ auth()->user()->name }} </h2>
    <a href="keranjang">Keranjang</a>

    <div class="bg-white">
        <div class="mx-auto max-w-2xl px-4 py-16 sm:px-6 sm:py-24 lg:max-w-7xl lg:px-8">
          <h2 class="text-2xl font-bold tracking-tight text-gray-900">Semua Produk</h2>

          <div class="mt-6 grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8">

            @foreach ($produk as $data)
                <div class="group relative">
                  <div class="aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-md bg-gray-200 lg:aspect-none group-hover:opacity-75 lg:h-80">
                    <img src="/storage/images/{{ $data->foto }}" alt="Front of men&#039;s Basic Tee in black." class="h-full w-full object-cover object-center lg:h-full lg:w-full">
                  </div>
                  <div class="mt-4 flex justify-between">
                    <div>
                      <h3 class="text-sm text-gray-700">
                        <a href="produk/{{ $data->id }}">
                          <span aria-hidden="true" class="absolute inset-0"></span>
                          {{ $data->nama }}
                        </a>
                      </h3>
                      <p class="mt-1 text-sm text-gray-500">Black</p>
                    </div>
                    <p class="text-sm font-medium text-gray-900">Rp. {{ $data->harga }}</p>
                  </div>
                </div>
            @endforeach


            <!-- More products... -->
          </div>
        </div>
      </div>

    <a href="/logout" onclick="return confirm('yalin ingin keluarrr')">Logout</a>


</body>
</html>
