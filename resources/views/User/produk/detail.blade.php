<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://cdn.tailwindcss.com"></script>
    <title>Document</title>
</head>
<body>

    <div class="">
        <a href="keranjang/{{ $produk->id }}">Kerjanjang</a>
        <img src="/storage/images/{{ $produk->foto }}" alt="">
        <p>{{ $produk->nama }}</p>
        <p>{{ $produk->harga }}</p>
        <p>{{ $produk->stok }}</p>
        <p>{{ $produk->deskripsi }}</p>
    </div>
</body>
</html>
