<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://cdn.tailwindcss.com"></script>
    <title>Document</title>
</head>
<body>
    <h1>Keranjang</h1>
    <div class="mt-10">
        @foreach ($user->produk as $produk)
        <div class="flex flex-row gap-5 my-3">
            <img src="/storage/images/{{ $produk->foto }}" width="100px" alt="">
            <div class="">

                <p class=" text-xl font-medium">{{ $produk->nama }}</p>
                <p>Rp.{{ $produk->harga }}</p>
                <div class="flex flex-row gap-6 ">
                    <div class="flex flex-row gap-2">
                        <a href="keranjang/{{ $produk->id }}/editMin">-</a>
                        <p>{{ $produk->pivot->qty }}</p>
                        <a href="keranjang/{{ $produk->id }}/editPlus">+</a>
                    </div>
                    <a href="keranjang/{{ $produk->id }}/delete">Del</a>
                </div>
            </div>
        </div>

        @endforeach
        <p class=" text-xl">Total: Rp.{{ $total }}</p>
        <a href="#">Checkout</a>
        <a href="dashboard">Dashboard</a>
    </div>
</body>
</html>
