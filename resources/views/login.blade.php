<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <script src="https://cdn.tailwindcss.com"></script>
</head>
<body>
    <div class=" w-96 mx-auto ">
        <div class="w-max mx-auto ">

            <h2 class="text-pink-400 text-4xl font-semibold mb-10">Login Page</h2>

            <form action="/login" method="post">
                @csrf
                <label for="name">Name</label><br>
                <input type="text" class="border w-max" name="name" id="name"><br>

                <label for="password">Password</label><br>
                <input type="password" class="border w-max" name="password" id="password"><br>
                @error('name')
                <p>Silahkan periksa akun kembali</p>
                @enderror

                <button type="submit" class="bg-red-400 mt-5">Login</button>
            </form>
        </div>
    </div>
</body>
</html>
