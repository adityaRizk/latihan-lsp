<?php

use App\Http\Controllers\Admin\IndexController as AdminIndexController;
use App\Http\Controllers\Admin\ProdukController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\User\IndexController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', [LoginController::class,'login'])->middleware('guest');
Route::prefix('user')->middleware(['CekRole:user'])->controller(IndexController::class)->group(function () {

    Route::get('/dashboard','index');

    Route::prefix('produk')->group(function () {

        Route::get('/{id}','show');
        Route::get('/keranjang/{id}','masukKeranjang');
    });
    Route::get('/keranjang','keranjang');
    Route::get('/keranjang/{id}/delete','deleteKeranjang');
    Route::get('/keranjang/{id}/editMin','KeranjangMin');
    Route::get('/keranjang/{id}/editPlus','KeranjangPlus');
});

Route::prefix('admin')->group(function () {

    Route::prefix('produk')->controller(ProdukController::class)->group(function () {
        Route::get('/','index');
        Route::get('/create','create');
        Route::get('/edit/{id}','edit');
        Route::post('/create','store');
        Route::put('/{id}','update');
        Route::delete('/{id}','destroy');
    });

    Route::controller(AdminIndexController::class)->middleware(['CekRole:admin'])->group(function () {
        Route::get('/dashboard', 'dashboard');
        Route::get('/', 'index');
        Route::get('/create', 'create');
        Route::get('/{id}', 'edit');
        Route::post('/create', 'store');
        Route::put('/{id}', 'update');
        Route::delete('/{id}', 'destroy');
    });
});

Route::post('/login', [LoginController::class,'authenticate']);
Route::get('/logout', [LoginController::class,'logout']);
